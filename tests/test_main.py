"""Tests for __main__."""
import argparse
import copy
import os
import unittest
from unittest import mock

from cki_lib.misc import EnvVarNotSetError

from tests import fakes
import triggers.__main__ as main_module


class TestMain(unittest.TestCase):
    """Tests for triggers.__main__.main()."""

    @mock.patch.dict('triggers.TRIGGERS', {'fake-trigger': fakes})
    @mock.patch('gitlab.Gitlab')
    @mock.patch('builtins.open', new_callable=mock.mock_open, read_data='{}')
    @mock.patch('argparse.ArgumentParser.parse_args')
    @mock.patch('cki_lib.cki_pipeline.trigger_multiple')
    def test_main_calls(self, mock_trigger, mock_argparse, mock_file,
                        mock_gitlab):
        """Verify main() calls all the functions it should."""
        mock_gitlab.return_value = fakes.FakeGitLab()
        mock_argparse.return_value = argparse.Namespace(
            trigger='fake-trigger',
            config=None,
            private_token='GITLAB_PRIVATE_TOKEN',
            kickstart=False,
            variables={},
        )
        mock_trigger.return_value = 'something'

        os.environ.update({'GITLAB_URL': 'http://gitlab.test',
                           'GITLAB_PRIVATE_TOKEN': 'private-token'})

        main_module.main()

        mock_trigger.assert_called_once()

        # Cleanup
        del os.environ['GITLAB_URL']
        del os.environ['GITLAB_PRIVATE_TOKEN']

    @mock.patch('gitlab.Gitlab')
    @mock.patch('builtins.open', new_callable=mock.mock_open, read_data='{}')
    @mock.patch('argparse.ArgumentParser.parse_args')
    @mock.patch('triggers.brew_trigger.poll_triggers')
    def test_poll_triggers(self, mock_triggers, mock_argparse, mock_file,
                           mock_gitlab):
        """Verify brew trigger polling is called."""
        mock_gitlab.return_value = fakes.FakeGitLab()
        mock_argparse.return_value = argparse.Namespace(
            trigger='brew',
            config=None,
            private_token='GITLAB_PRIVATE_TOKEN',
            kickstart=False,
            variables={},
        )

        os.environ.update({'GITLAB_URL': 'http://gitlab.test',
                           'GITLAB_PRIVATE_TOKEN': 'private-token'})

        main_module.main()

        mock_triggers.assert_called_once()

        # Cleanup
        del os.environ['GITLAB_URL']
        del os.environ['GITLAB_PRIVATE_TOKEN']

    @mock.patch.dict('triggers.TRIGGERS', {'fake-trigger': fakes})
    @mock.patch('gitlab.Gitlab')
    @mock.patch('builtins.open', new_callable=mock.mock_open, read_data='{}')
    @mock.patch('argparse.ArgumentParser.parse_args')
    @mock.patch('cki_lib.cki_pipeline.trigger_multiple')
    def test_required_envvars(self, mock_trigger, mock_argparse, mock_file,
                              mock_gitlab):
        """Verify EnvVarNotSet is raised if a required variable is missing."""
        mock_gitlab.return_value = fakes.FakeGitLab()
        mock_argparse.return_value = argparse.Namespace(
            trigger='fake-trigger',
            config=None,
            private_token='GITLAB_PRIVATE_TOKEN',
            kickstart=False
        )
        mock_trigger.return_value = 'something'

        variables = {'GITLAB_URL': 'http://gitlab.test',
                     'GITLAB_PRIVATE_TOKEN': 'private-token'}

        for variable in variables:
            reduced = copy.deepcopy(variables)
            del reduced[variable]
            with self.assertRaises(EnvVarNotSetError):
                main_module.main()
