"""Fake classes."""
# pylint: disable=too-few-public-methods

from unittest import mock


def load_triggers(gitlab_instance, config, kickstart):
    # pylint: disable=unused-argument
    """Fake load_triggers function."""
    return [{'trigger': 'fake'}]


class FakeGitLabCommits():
    """Fake gitlab commits."""

    def __init__(self):
        """Initialize."""
        self._commits = []

    def create(self, data):
        """Create a fake commit."""
        self._commits.append(data)

    def __getitem__(self, index):
        """Get a fake commit."""
        return self._commits[index]


class FakePipelineJob():
    """Fake pipeline job."""

    def __init__(self, job_id):
        """Initialize."""
        self.attributes = {'id': job_id}


class FakePipelineJobs():
    """Fake pipeline jobs."""

    def __init__(self):
        """Initialize."""
        self._jobs = []
        self.add_job(1)

    def clear(self):
        """Remove all jobs."""
        self._jobs = []

    def add_job(self, job_id):
        """Add a fake job."""
        self._jobs.append(FakePipelineJob(job_id))

    def list(self, page=None, per_page=None):
        # pylint: disable=unused-argument
        """List all fake jobs."""
        return self._jobs


class FakePipeline():
    """Fake pipeline."""

    def __init__(self, branch, token, variables, status):
        """Initialize."""
        self.jobs = FakePipelineJobs()
        self.attributes = {'status': status}
        for key, value in variables.items():
            self.attributes[key] = value

        self.branch = branch
        self.token = token
        self.variables = variables


class FakeProjectPipelines():
    """Fake project pipelines."""

    def __init__(self):
        """Initialize."""
        self._pipelines = []

    def add_new_pipeline(self, branch, token, variables, status):
        """Add a fake pipeline."""
        self._pipelines.append(
            FakePipeline(branch, token, variables, status)
        )

    def __getitem__(self, index):
        """Get a fake pipeline."""
        return self._pipelines[index]

    def list(self, **kwargs):
        """List all fake pipelines."""
        kwargs.pop('as_list')
        if not kwargs:
            return self._pipelines

        to_return = []
        for pipeline in self._pipelines:
            if pipeline.branch == kwargs.get('ref'):
                to_return.append(pipeline)
            elif pipeline.attributes['status'] == kwargs.get('status'):
                to_return.append(pipeline)

        return to_return


class FakeGitLabProject():
    """Fake gitlab project."""

    def __init__(self):
        """Initialize."""
        self.commits = FakeGitLabCommits()
        self.attributes = {'web_url': 'http://web-url'}
        self.pipelines = FakeProjectPipelines()
        self.manager = mock.MagicMock()

    def trigger_pipeline(self, branch, token, variables, status='pending'):
        """Add a fake pipeline."""
        self.pipelines.add_new_pipeline(branch, token, variables, status)


class FakeGitLab():
    """Fake gitlab."""

    def __init__(self):
        """Initialize."""
        self.projects = {}

    def add_project(self, project_name):
        """Add a fake project."""
        self.projects[project_name] = FakeGitLabProject()
