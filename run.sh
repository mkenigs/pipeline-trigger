#!/bin/bash
set -euo pipefail

if [ -v SCHEDULER_DATA_DIR ]; then
    DATA_DIR="${SCHEDULER_DATA_DIR}"
elif [ -v CI_PROJECT_DIR ]; then
    DATA_DIR="${CI_PROJECT_DIR}"
else
    DATA_DIR=/data
fi

if [ -v TRIGGER_CONFIG_URL ]; then
    TRIGGER_CONFIG_FILENAME="${TRIGGER_CONFIG_URL##*/}"
    curl \
        --retry 5 \
        --connect-timeout 30 \
        --location \
        --silent \
        --show-error \
        --output "${DATA_DIR}/${TRIGGER_CONFIG_FILENAME}" \
        "${TRIGGER_CONFIG_URL}"
fi

export START_PYTHON_TRIGGER="triggers ${TRIGGER_TYPE} ${TRIGGER_OPTIONS:-} -c ${DATA_DIR}/${TRIGGER_CONFIG_FILENAME}"
export LOG_NAME="${TRIGGER_TYPE}"
if [ -v TRIGGER_SCHEDULE ]; then
    # shellcheck disable=SC2086
    python3 -m $START_PYTHON_TRIGGER 2>&1 | tee -a "/logs/${LOG_NAME}.log"
else
    exec cki_entrypoint.sh
fi
