"""Trigger for Brew/Koji builds using UMB.

See https://pagure.io/fedora-ci/messages for details.
"""
import copy
import os
import re

from cki_lib import cki_pipeline
from cki_lib import config_tree
from cki_lib import logger
from cki_lib import messagequeue
from cki_lib import metrics
from cki_lib import misc
from pipeline_tools import brew_trigger
import prometheus_client as prometheus

LOGGER = logger.get_logger(__name__)

METRIC_PIPELINE_TRIGGERED = prometheus.Counter(
    'pipeline_triggered', 'Number of pipelines triggered')


def trigger_pipeline(gitlab_instance, trigger_to_use):
    """Process GitLab trigger requests."""
    LOGGER.debug("Triggering pipeline: gitlab_instance=%s trigger_to_use=%s",
                 gitlab_instance, trigger_to_use)
    if not misc.is_production():
        LOGGER.info("Staging environment, not triggering pipeline.")
        return
    try:
        cki_pipeline.trigger_multiple(gitlab_instance, [trigger_to_use])
    except Exception:  # pylint: disable=broad-except
        LOGGER.exception('Unable to trigger pipeline for %s',
                         trigger_to_use)
        raise

    METRIC_PIPELINE_TRIGGERED.inc()


def sanity_check(properties):
    """Verify the message is what we want.

    Args:
        properties: A dictionary of message properties.

    Returns:
        False if the message is "bad", True if we should continue processing.
    """
    if properties.get('attribute') != 'state':
        return False

    # We are not interested in messages that don't mark a completed task. Also,
    # official koji builds like to use numbers.
    if properties.get('new') not in ['CLOSED', 1]:
        return False

    # We are only interested in completed builds. koji official messgaes are
    # special again.
    if properties.get('method') != 'build' and 'build_id' not in properties:
        return False

    return True


def process_copr(triggers, message):
    """Process a message from COPR."""
    # Exclude empty automated nightly builds
    if not message.get('pkg') or not message.get('version'):
        return None

    nvr = '{}-{}'.format(message['pkg'], message['version'])
    if message.get('status') != 1:
        LOGGER.debug('COPR build for %s not successful', nvr)
        return None

    LOGGER.info('COPR build for %s found', nvr)
    copr = '{}/{}'.format(message['owner'], message['copr'])

    trigger_to_use = None
    for trigger in triggers:
        if 'result_pipe' in trigger:
            continue  # This is not the trigger we are looking for

        if re.search(r'^{}-\d+[.-](\S+[.-])+{}'.format(trigger['package_name'],
                                                       trigger['rpm_release']),
                     nvr):
            if copr in trigger.get('.coprs', []):
                trigger_to_use = trigger.copy()
                break
    if not trigger_to_use:
        LOGGER.info('COPR: Pipeline for %s not configured', nvr)
        return None

    architecture = message['chroot'].split('-')[-1]

    trigger_to_use['owner'] = message['user']
    trigger_to_use['nvr'] = nvr
    trigger_to_use['copr_build'] = str(message['build'])
    trigger_to_use['title'] = f'COPR: {nvr}: {architecture}'
    trigger_to_use['architectures'] = architecture
    trigger_to_use['repo_name'] = copr
    trigger_to_use['cki_pipeline_type'] = 'copr'
    trigger_to_use['discovery_time'] = misc.utc_now_iso()

    trigger_to_use = config_tree.clean_config(trigger_to_use)
    return trigger_to_use


def process_message(brew_config, server_section,
                    message):
    """Process a message from Koji or Brew."""
    if 'task_id' in message:
        task_id = message['task_id']  # Koji official builds
    else:
        task_id = message['id']  # Koji scratch builds and Brew
    if not task_id:   # Some koji builds don't have related tasks
        return None
    LOGGER.info('%s: A build completed!', task_id)

    if not message.get('request'):
        LOGGER.info('%s: Task doesn\'t have a build request, ignoring',
                    task_id)
        return None

    trigger_to_use = brew_trigger.get_brew_trigger_variables(
        task_id, brew_config, server_section)

    return trigger_to_use


def get_triggers_from_config(brew_config):
    """Transform config file to individual triggers."""
    triggers = []
    for key, value in \
            config_tree.process_config_tree(brew_config).items():
        trigger = copy.deepcopy(value)
        trigger['name'] = key
        triggers.append(trigger)
    return triggers


class AMQPMessageReceiver:
    """Receive AMQP messages and process them to generate triggers."""

    def __init__(self, triggers, brew_config, gitlab_instance):
        """Initialize message receiver."""
        self.triggers = triggers
        self.brew_config = brew_config
        self.gitlab_instance = gitlab_instance

        # Callback metrics values
        self._callback_enter_ts = None
        self._callback_exit_ts = None

    def process_umb(self, message, headers):
        """Process received message from UMB messaging bus."""
        properties = headers['message-amqp10-properties']

        if 'info' in message:
            message = message['info']

        if sanity_check(properties):
            return process_message(self.brew_config, '.amqp', message)
        return None

    def process_fedmsg(self, message):
        """Process received message from Fedora messaging bus."""
        if 'copr' in message:
            return process_copr(self.triggers, message)
        if sanity_check(message):
            if 'info' in message:  # Only scratch builds
                message = message['info']
            return process_message(self.brew_config, '.amqp091', message)
        return None

    def callback(self, body, headers, **_):
        """Process received messages."""
        bridge_name = headers['message-amqp-bridge-name']

        if bridge_name == 'fedmsg':
            trigger_to_use = self.process_fedmsg(body)
        elif bridge_name == 'umb':
            trigger_to_use = self.process_umb(body, headers)
        else:
            raise Exception("Can't process %s" % bridge_name)

        if trigger_to_use:
            trigger_pipeline(self.gitlab_instance, trigger_to_use)


def poll_triggers(gitlab_instance, brew_config):
    """Create the actual triggers and start the receiver."""
    metrics.prometheus_init()

    triggers = get_triggers_from_config(brew_config)
    receiver = AMQPMessageReceiver(triggers, brew_config, gitlab_instance)

    exchange = os.environ['RABBITMQ_EXCHANGE']
    queue = os.environ['RABBITMQ_QUEUE']
    routing_keys = os.environ['RABBITMQ_ROUTING_KEYS'].split()

    connection = messagequeue.MessageQueue()
    connection.consume_messages(
        exchange, routing_keys, receiver.callback,
        callback_kwargs=True, queue_name=queue
    )
