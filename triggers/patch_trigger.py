"""Pipeline trigger for patches from Patchwork2 instances.

1. Find the last pipeline for chosen project and get the 'event_id' and
   'skipped_series' variables. 'event_id' is used in the PW2 API call to get
   new series and 'skipped_series' are series which were skipped in the
   previous runs because the cover letter was delayed.
2. Get needed data about both previously skipped series and the new ones.
3. Check if any of the series need to be delayed. Save the info about them
   into the newly constructed 'skipped_series' and skip them.
4. Profit $$$.
"""
import os
import os.path
import re

from cki_lib import cki_pipeline
from cki_lib import config_tree
from cki_lib import gitlab
from cki_lib.logger import get_logger
from cki_lib.misc import utc_now_iso
from cki_lib.session import get_session

from . import utils

LOGGER = get_logger(__name__)
SESSION = get_session(__name__)


def seen_series(series_list, series_id):
    """Check is series with given series_id (string) are already in the list.

    We don't want to trigger two same pipelines if multiple extra (e.g. 3/2 and
    4/2) patches arrive.
    """
    return next((series for series in series_list
                 if series.series_id == series_id),
                False)


def get_patch_data(patchwork_url, patch_web_url):
    """Get the data associated with the patch.

    Args:
        patchwork_url: Base URL of the Patchwork instance.
        patch_web_url: Direct URL of the patch.

    Returns:
        2 strings representing the message ID and subject.
    """
    patch_id = os.path.basename(patch_web_url.rstrip('/'))

    response = SESSION.get('{}/api/patches/{}'.format(patchwork_url,
                                                      patch_id))
    if response.status_code != 200:
        raise Exception(
            'Unexpected response {} from {}/api/patches/{} !'.format(
                response.status_code, patchwork_url, patch_id
            ))

    patch_data = response.json()

    # PW API is funny sometimes and both 'Message-Id' and 'Message-ID' are
    # valid keys
    try:
        message_id = patch_data['headers']['Message-Id']
    except KeyError:
        message_id = patch_data['headers']['Message-ID']

    subject = re.sub(r'\r?\n[ \t]', ' ', patch_data['headers']['Subject'])

    return message_id, subject


def get_series_data(patchwork_url, series_url, event_id):
    """Get a list of patches from the series and associated data we need.

    Args:
        patchwork_url: Base URL of the Patchwork instance.
        series_url:    URL of the series detail in the Patchwork API.
        event_id:      Event ID associated with the completed series.

    Returns:
        A utils.SeriesData object.
    """
    patches = []
    emails = set()

    response = SESSION.get(series_url)
    if response.status_code != 200:
        raise Exception('Unexpected response {} from {} !'.format(
            response.status_code, series_url
        ))

    series_data = response.json()
    series_name = series_data['name'] if series_data['name'] else ''
    if not series_data['received_all'] or \
            utils.PATCH_SKIP.search(series_name):
        LOGGER.info('Skipping %d: %s', series_data['id'], series_name)
        return None

    for patch in series_data['patches']:
        if not utils.PATCH_SKIP.search(patch['name']):
            patches.append(patch['web_url'])

            # Nested patch objects in series detail don't contain headers so we
            # need to get the actual patch detail...
            patch_response = SESSION.get('{}/api/patches/{}'.format(
                patchwork_url, patch['id']
            ))
            if patch_response.status_code != 200:
                raise Exception(
                    'Unexpected response {} when getting patch {} !'.format(
                        patch_response.status_code, patch['id']
                    )
                )
            patch_detail = patch_response.json()
            emails |= utils.get_emails_from_headers(patch_detail['headers'])

    if not patches:
        return None

    message_id, subject = get_patch_data(patchwork_url, patches[-1])

    retrieved_series = utils.SeriesData(
        patches=patches,
        emails=emails,
        subject=subject,
        message_id=message_id,
        last_tested=str(event_id),
        series_id=str(series_data['id']),
        submitter=series_data['submitter']['email']
    )
    if series_data['cover_letter']:
        retrieved_series.cover = series_data['cover_letter']['mbox']

    LOGGER.info('%s', retrieved_series)
    return retrieved_series


def get_series_by_event(patchwork_url, project, last_event_id):
    """Retrieve a list of new series data.

    Check out new series from the events API endpoint instead of checking the
    series endpoint to make sure nothing gets missed -- sometimes people post
    subsequent patches to already existing series and we do want to retest
    them, but these would be missed by querying the series endpoint.

    Args:
        patchwork_url: Base URL of the Patchwork instance.
        project:       Name of the Patchwork project to query.
        last_event_id: ID of the last event we saw before.

    Returns:
        A list of dictionaries representing all new series.
    """
    series_list = []
    found_last_event = False
    base_api_filter = '/api/events?category=series-completed'
    events_endpoint = '{}{}&project={}'.format(patchwork_url,
                                               base_api_filter,
                                               project)

    while not found_last_event:
        response = SESSION.get(events_endpoint)
        if response.status_code != 200:
            raise Exception('Unexpected response {} from {} !'.format(
                response.status_code, events_endpoint
            ))

        response_data = response.json()
        # If there is a single event returned we get a dict, not a list with a
        # single element. Fix this inconsistency for easier processing.
        if not isinstance(response_data, list):
            response_data = [response_data]

        for event in response_data:
            event_id = event['id']
            if event_id == last_event_id:
                # We saw this before
                found_last_event = True
                break

            series_id = event['payload']['series']['id']
            if seen_series(series_list, str(series_id)):
                LOGGER.debug('Skipping already seen series %d', series_id)
                continue

            LOGGER.info('Discovered new series %d', series_id)
            new_series = get_series_data(
                patchwork_url,
                '{}/api/series/{}'.format(patchwork_url, series_id),
                event_id
            )
            if new_series:
                series_list.append(new_series)

        link = response.headers.get('Link')
        if link:
            match = re.match('<(.*)>; rel="next"', link)
            if match:
                events_endpoint = match.group(1)
        else:
            # No more events to check. This shouldn't happen and we should
            # always jump out by finding the last_event_id; but if it does
            # happen it shouldn't cause any problems.
            break

    return series_list


def retry_skipped(patchwork_url, skipped_series, last_event_id):
    """Requery previously skipped series and return their data.

    Args:
        patchwork_url:  Base URL of the Patchwork instance.
        skipped_series: A list of skipped series IDs.
        last_event_id:  ID of the last previously tested event. Use this to
                        override the old event ID associated with the retried
                        series to cause retesting of other series.

    Returns: A list of series data for previously skipped series.
    """
    series_list = []
    LOGGER.info('Requerying previously skipped series: %s', skipped_series)
    for series_id in skipped_series:
        series_data = get_series_data(
            patchwork_url,
            '{}/api/series/{}'.format(patchwork_url, series_id),
            last_event_id
        )
        if series_data:
            series_list.append(series_data)

    return series_list


def get_kickstart_series(url, project):
    """Get last existing series to kickstart the pipeline with.

    Args:
        url:     Base URL of Patchwork instance.
        project: Name of the Patchwork project to query.

    Returns:
        A list containing a single SeriesData object.
    """
    base_api_filter = '/api/events?category=series-completed'
    events_endpoint = '{}{}&project={}'.format(url, base_api_filter, project)
    response = SESSION.get(events_endpoint)
    if response.status_code != 200:
        raise Exception('Unexpected response {} from {} !'.format(
            response.status_code, events_endpoint
        ))

    # Get the newest series
    try:
        event = response.json()[0]
    except IndexError:
        raise Exception('No series found for {} and {}!'.format(url, project)) from None

    event_id = event['id']
    series_id = event['payload']['series']['id']

    series_endpoint = f'{url}/api/series/{series_id}'
    response = SESSION.get(series_endpoint)
    if response.status_code != 200:
        raise Exception('Unexpected response {} from {} !'.format(
            response.status_code, series_endpoint
        ))
    series = response.json()

    patches = [patch['web_url'] for patch in series['patches']]

    message_id, subject = get_patch_data(url, patches[-1])
    retrieved_series = utils.SeriesData(
        patches=patches,
        emails=set([]),
        subject=subject,
        message_id=message_id,
        last_tested=str(event_id),
        series_id=str(series['id']),
        submitter=series['submitter']['email']
    )
    if series['cover_letter']:
        retrieved_series.cover = series['cover_letter']['mbox']

    LOGGER.info('Kickstarting %s (%s) with %s', project, url,
                retrieved_series)
    return [retrieved_series]


def is_cover_missing(patchwork_url, series):
    """Check if an expected cover letter is missing.

    Check that if a cover letter was expected, it already arrived by the time
    we queried the series. Patchwork2 currently offers no way to verify this so
    we must check it manually.

    Args:
        patchwork_url: Base URL of the Patchwork2 instance.
        series:        A SeriesData object.

    Returns: True if a cover letter is expected and we don't have it,
             False otherwise.
    """
    if series.cover or len(series.patches) == 1:
        return False

    first_patch_url = series.patches[0].strip('/')
    api_url = '{}/api/patches/{}'.format(patchwork_url,
                                         first_patch_url.split('/')[-1])
    response = SESSION.get(api_url)
    if response.status_code != 200:
        raise Exception('Unexpected response {} from {} !'.format(
            response.status_code, api_url
        ))
    response_data = response.json()

    in_reply_to = response_data.get('headers', {}).get('In-Reply-To')
    if not in_reply_to:
        return False

    return True


def get_last_event_and_skipped_series(project, data):
    """Retrieve the event ID and skipped series associated with last series.

    The skipped series are for re-checking the series for which a cover letter
    didn't arrive when they were originally checked.
    """
    for pipeline in project.pipelines.list(
            as_list=False, ref=data['cki_pipeline_branch']):
        variables = gitlab.get_variables(pipeline)
        if variables.get('cki_pipeline_type') == 'patchwork':
            url = variables.get('patchwork_url')
            patchwork_project = variables.get('patchwork_project')
            if (url, patchwork_project) != (data['patchwork_url'], data['patchwork_project']):
                # We found the last patch from a different source
                continue

            event_id = variables.get('event_id')
            skipped = variables.get('skipped_series', '')
            if event_id:
                LOGGER.info('Last series came from event %s', event_id)

                skipped_series = skipped.split()
                LOGGER.info('Previously skipped series: %s',
                            skipped_series)
                return (int(event_id), skipped_series)

    raise Exception('No tested patches found!')


def load_triggers(gitlab_instance, patches_config, kickstart):
    """Gather all triggers."""
    triggers = []
    for key, value in patches_config.items():
        value = value.copy()
        project = cki_pipeline.pipeline_project(gitlab_instance, value)

        value['patchwork_url'] = value['patchwork']['url']
        value['patchwork_project'] = value['patchwork']['project']
        del value['patchwork']

        value['cki_pipeline_type'] = 'patchwork'

        value['name'] = key
        value['commit_hash'] = utils.get_commit_hash(value['git_url'],
                                                     f'refs/heads/{value["branch"]}')
        if value['commit_hash'] is None:
            continue

        if kickstart:
            new_series = get_kickstart_series(value['patchwork_url'],
                                              value['patchwork_project'])
            newly_skipped_series = []
        else:
            last_event_id, skipped_series = get_last_event_and_skipped_series(
                project, value
            )
            new_series = retry_skipped(value['patchwork_url'],
                                       skipped_series,
                                       last_event_id)
            found_series = get_series_by_event(value['patchwork_url'],
                                               value['patchwork_project'],
                                               last_event_id)
            # We get the newest series first which is not desired if we are to
            # keep the order correct, reverse them.
            found_series.reverse()
            new_series.extend(found_series)

            newly_skipped_series = []
            for series in new_series:
                if is_cover_missing(value['patchwork_url'], series):
                    newly_skipped_series.append(series.series_id)

        for series in new_series:
            if series.series_id in newly_skipped_series:
                continue

            trigger = value.copy()
            trigger['skipped_series'] = ' '.join(newly_skipped_series)
            trigger['patch_urls'] = ' '.join([os.path.join(patch_url, 'mbox/')
                                              for patch_url in series.patches])
            trigger['submitter'] = series.submitter

            if kickstart:
                trigger['send_report_to_upstream'] = False
                trigger['send_report_on_success'] = False
                trigger['mail_to'] = ''
                trigger['title'] = 'Patch kickstart: {}: {}'.format(
                    trigger['name'], series.subject
                )
            else:
                trigger.setdefault('mail_to', trigger.get('mail_to', ''))
                if trigger.get('send_report_to_upstream', False):
                    trigger['mail_to'] += ', ' + ', '.join(series.emails)
                trigger['title'] = 'Patch: {}: {}'.format(trigger['name'],
                                                          series.subject)

            trigger['send_report_to_upstream'] = trigger.get('send_report_to_upstream', False)
            trigger['send_report_on_success'] = trigger.get('send_report_on_success', False)
            trigger['message_id'] = series.message_id
            trigger['subject'] = 'Re: ' + series.subject
            trigger['event_id'] = series.last_tested
            trigger['cover_letter'] = series.cover
            trigger['discovery_time'] = utc_now_iso()

            triggers.append(config_tree.clean_config(trigger))
    return triggers
